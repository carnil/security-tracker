A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
asterisk (apo)
--
commons-configuration
--
connman (carnil)
--
freecad (aron)
--
gdk-pixbuf (carnil)
--
linux (carnil)
  Wait until more issues have piled up, though try to regulary rebase for point
  releases to more recent v5.10.y versions
--
maven-shared-utils
--
netatalk
  open regression with MacOS, tentative patch not yet merged upstream
--
nodejs
--
php-horde-mime-viewer
--
php-horde-turba
--
rails
--
rpki-client
  new 7.6 release required libretls, which isn't in Bullseye
--
ruby-image-processing
--
ruby-rack
--
ruby-tzinfo
--
salt
--
sofia-sip
  Maintainer proposed debdiff, though as rebuild of the testing version
--
sox
  patch needed for CVE-2021-40426, check with upstream
--
